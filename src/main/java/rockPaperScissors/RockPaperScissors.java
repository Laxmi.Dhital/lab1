package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code new RockPaperScissors(). Then it calls the run()
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        Scanner input = new Scanner(System.in);
        
        while(true) {
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String myMove = input.nextLine();
    
            //passer på at jeg har riktig input
            if(!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")){
                System.out.println("I do not understand cardboard. Could you try again?");
            }
    
            //legger til motspillerens automatiske steg
            else{
                int random = (int)(Math.random() * 3);
                String ComputerMove = "";
                if (random == 0){
                    ComputerMove = "rock";
                }
                else if (random == 1){
                    ComputerMove = "paper";
                }
                else{
                    ComputerMove = "scissors";
                }
    
                //Sjekker det ble en vinner, taper eller om det ble uavgjort
                if(myMove.equals(ComputerMove)){
                    System.out.println("Human chose " + myMove + ", computer chose "+ ComputerMove + ". It's a tie!");
                }
                else if((myMove.equals("rock") && ComputerMove.equals("scissors")) 
                | (myMove.equals("scissors") && ComputerMove.equals("paper")) 
                | (myMove.equals("paper") && ComputerMove.equals("rock")) ){
                    System.out.println("Human chose " + myMove + ", computer chose "+ ComputerMove + ". Human wins!");
                    humanScore += 1;
                }
                else{
                    System.out.println("Human chose " + myMove + ", computer chose "+ ComputerMove + ". Computer wins!");
                    computerScore += 1;
                }
            }

        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        roundCounter += 1;

            //Continue answer
        System.out.println("Do you wish to continue playing? (y/n)?");
        String answer = input.nextLine();
        
        if(answer.equals("y")){
            continue;
            }
        else if(answer.equals("n")){
            System.out.println("Bye bye :) ");
            break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
